const { factorial, divisibility_check } = require('../src/util.js');
// here import the functions that we will use as exported from util.js

const { expect, assert } = require('chai');

// ASSERT vs EXPECT
/*
	ASSERT: Fails fast, aborting the current function.
	EXPECT: Continues after the failure.
*/

describe('test_fun_factorials', ()=>{
	it('test_fun_factorial_5!_is_120', () => {
		const product = factorial(5);
		expect(product).to.equal(120);
	})

	it('test_fun_factorial_1!_is_1', () => {
		const product = factorial(1);
		assert.equal(product, 1);
	})	

//  Create a new test cases for testing different functions

	// 1. In your test.js, create test cases in the test fun factorials test suite to check if the factorial function's result for 0!, 4!, and 10! using assert and/or expect.
	it('test_fun_factorial_0!_is_1', () => {
		// Same function will be used, this is the benefit of function, it is reusable
		const product = factorial(0); // The argument serves as the input value to be passed in the n parameter of the factorial function of util.js
		assert.equal(product, 1); 
	});

	it('test_fun_factorial_4!_is_24', () => {
		const product = factorial(4);
		expect(product).to.equal(24);
	});

	it('test_fun_factorial_10!_is_3628800', () => {
		const product = factorial(10);
		expect(product).to.equal(3628800);
	})


})

// describe() string name does not affect the string name of it()
describe('test_n_is_divisible_by_5_and_7', ()=>{

	it('test_100_is_divisible_by_5',() => {
		const divisibility  = divisibility_check(100);
		expect(divisibility).to.equal(true);
	})
	it('test_49_is_divisible_by_7',() => {
		const divisibility  = divisibility_check(49);
		expect(divisibility).to.equal(true);
	})
	it('test_30_is_divisible_by_5',() => {
		const divisibility  = divisibility_check(5);
		expect(divisibility).to.equal(true);
	})
	it('test_56_is_divisible_by_7',() => {
		const divisibility  = divisibility_check(7);
		expect(divisibility).to.equal(true);
	})



})
