function factorial(n){
	if(n===0) return 1;
	if(n===1) return 1;
	return n * factorial(n-1);
}


// Instead of div_check I used divisilibility_check so it could be more understandable
// In your util.js, create a function called div_check in util.js that checks a number if it is divisible by 5 or 7.
function divisibility_check(n){
	/*If the number received is divisible by 5, return true.
	If the number received is divisible by 7, return true.
	Return false if otherwise
	*/
	if(n%5 === 0) return true
	if(n%7 === 0) return true
	return false
}

module.exports = {
	factorial: factorial,
	divisibility_check: divisibility_check
}
